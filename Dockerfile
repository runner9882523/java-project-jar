FROM openjdk
COPY target/*.jar /
EXPOSE 8080
ENTRYPOINT ["java","-jar","/uuid_timestamp_pub##1.0.0.jar"]
